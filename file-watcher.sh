#!/bin/bash

XDEBUG='/usr/bin/env LOG_LEVEL=debug php -dxdebug.mode=debug -dxdebug.client_port=9003 -dxdebug.start_with_request=yes'

function e {
    fn=$1
    tests=$(find tests/Unit \
      \( \
        -iname $(basename ${fn%.*})Test.php \
        -or \
        -name $(basename ${fn%.*}).php   \
      \) \
      -and -not -path "*.trash*"  \
    ) 
    echo ${tests:-tests}
} 

while true; do
    #FILE=$(inotifywait --recursive --exclude=".*.*sw*"  ./app ./tests  --format "%w%f" -e close_write) &&
    #fswatch  -e ".*" -i "\\.php$" --event=Updated app tests
    FILE=$(fswatch -1  -e ".*" -i "\\.php$" --event=Updated tests *.php)
    clear
    #./vendor/bin/sail artisan test
    echo "-- $FILE"
    tests=$(e $FILE)
    echo "tests: ${tests}"

    echo "run: ${XDEBUG} ./vendor/bin/phpunit --testdox $tests"
    ${XDEBUG} ./vendor/bin/phpunit --testdox $tests
done
