# Minimal wrapping of Twilio SMS Sending in a WordPress Plugin

## Local Development

This plugin has a wp-env setup. You can start it with a full WordPress env from your command line like:
```
$ wp-env start
```

After that, open the plugin setting in the wp-admin area. On a mac you might just call:
```
$ open http://localhost:1611/wp-admin/options-general.php?page=healy-twilio-sms
```

The default admin account with the WordPress wp-env set-up is:
 - username: admin
 - password: password

## Demo
From the plugin Admin page you can send SMS messages for testing purpose. These
will only work with a proper Twilio account and only for the numbers which the
Twilio account is configuered for. Configuration of the capabilities of the
Twilio services itself lies outside the scope of this plugin.

## Programming API

This plugin is soley about sending SMS. Its api is simply calling a WordPress
action from anywhere in your code, theme or plugin, doesn't matter:

```
do_action('send-healy-twilio-sms', '+49 172 1234567', 'your message here');
```

## Configuration (& Security)

You need your twilio account credentials. You can enter them on plugin admin settings page you find in the menu. For future live deployments you have the option to not put them into the database, but inject them into the process envionment or by defining them as constants.

For local development it is easiest to put them in your local `.wp-env.override.json` file like this:

```js
{
  "config": {
      "HEALY_TWILIO_SMS_ACCOUNT_SID": "your account here",
      "HEALY_TWILIO_SMS_AUTH_TOKEN": "your account auth token here",
      "HEALY_TWILIO_SMS_SENDER_NO": "+49 1234 1234567"
  }
}
```
