<?php

declare(strict_types=1);

namespace Healy\Twilio\Sms\Plugin;

/*
   Plugin Name: Healy World Twilio Sms
   Plugin URI: https://healy.world/
   description: Use Twilio to send SMS from your wordpress site
   Version: 0.0.1
   Author: Dirk Lüsebrink
   Author URI: https://healy.world
   License: MIT
*/
defined('ABSPATH') or die('script kiddies go home!');

// autoloading for composer dependencies. Check for file existance first, could
// be we are running inside a wordpress composer install environment in which
// case the autoload file is elsewhere
if (is_readable(__DIR__ . '/vendor/autoload.php')) {
    require __DIR__ . '/vendor/autoload.php';
}

require_once __DIR__ . '/log-helper.php';
use function Crux\WordPress\{ log, err, dbg };

// the non-admin functionionality of this plugin
require_once __DIR__ . '/plugin.php';

// admin menu and settings page setup
require_once __DIR__ . '/admin.php';

// run on plugin activation
register_activation_hook(__FILE__, static function (): void {
    dbg('plugin activation hook: ' . __FILE__);
});

// run on plugin de-activation
register_deactivation_hook(__FILE__, static function (): void {
    dbg('plugin de-activation hook: ' . __FILE__);
});
