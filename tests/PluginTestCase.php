<?php

declare(strict_types=1);

namespace Healy\Twilio\Sms\Plugin\Test;

use PHPUnit\Framework\TestCase;

define('WP_DEBUG', true);
ini_set("error_log", 'tests.log');

abstract class PluginTestCase extends TestCase
{
}
