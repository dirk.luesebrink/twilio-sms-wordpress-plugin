<?php

declare(strict_types=1);

namespace Healy\Twilio\Sms\Plugin;

require_once __DIR__ . '/log-helper.php';
require_once __DIR__ . '/form-helper.php';

use function Crux\WordPress\{ log, err, dbg };
use function Crux\WordPress\FormHelper\{
    init as form_helper_init,
    action_slug,
    render_input,
    add_save_action_callback
};

const DEMO_FORM = 'healy-twilio-demo';

add_action('admin_init', function (): void {
    dbg('admin init: ' . __FILE__);
    form_helper_init(PLUGIN_NAME);

    form_helper_init(DEMO_FORM);
    add_save_action_callback(DEMO_FORM, fn (array $fields) => send_demo_sms($fields));
});

function send_demo_sms(array $fields): void
{
    dbg('send demo sms: ' . json_encode($fields));
    do_action(
        'send-healy-twilio-sms',
        $fields['demo-number'] ?? '',
        $fields['demo-message'] ?? ''
    );
}

// register the plugin settings admin menu
add_action("admin_menu", function (): void {
    dbg('admin menu init: ' . __FILE__);

    add_submenu_page(
        "options-general.php",          // Which menu parent
        "Healy Twilio SMS",             // Page title
        "Healy Twilio SMS",             // Menu title
        "manage_options",               // Minimum capability (manage_options is an easy way to target Admins)
        "healy-twilio-sms",             // Menu slug
        fn() => render_settings_page()  // Callback that prints the markup
    );
});

// render html markup for the settings page
function render_settings_page(): void
{
    dbg('render_settings_page');
    if (isset($_GET['status']) && $_GET['status'] === 'success') {
        render_success_message();
    }

    render_setting_form(); // $settings);
    render_send_demo_form();
}

function render_success_message(): void { ?>
    <div id="message" class="updated notice is-dismissible">
        <p><?php _e("Healy Twilio SMS Settings updated!", "healy-twilio-sms-plugin"); ?></p>
        <button type="button" class="notice-dismiss">
            <span class="screen-reader-text"><?php _e("Dismiss this notice.", "healy-twilio-sms-plugin"); ?></span>
        </button>
    </div>
<?php }

function render_setting_form(): void { ?>
    <h3>Healy Twilio SMS</h3>
    <form method="post" action="<?php echo admin_url('admin-post.php'); ?>">
        <?php settings_fields(PLUGIN_NAME); ?>
        <input type="hidden" name="action" value="<?php echo action_slug(PLUGIN_NAME); ?>" />
        <table> <!-- class="form-table" role="presentation"-->
            <?php
                render_input(PLUGIN_NAME, "Twilio Account SID", "account_sid");
                render_input(PLUGIN_NAME, "Twilio Auth Token", "auth_token");
                render_input(PLUGIN_NAME, "Twilio Number", "sender_no");
            ?>
            <tr>
                <td></td>
                <td>
                    <input 
                        class="button button-primary" 
                        type="submit" value="<?php _e("Save", "healy-twilio-sms-plugin"); ?>" 
                    />
                </td>
            </tr>
        </table>
    </form>
<?php }

function render_send_demo_form(): void { ?>
    <h3>Send a Test SMS</h3>
    <form method="post" action="<?php echo admin_url('admin-post.php'); ?>">
        <?php settings_fields(DEMO_FORM); ?>
        <input type="hidden" name="action" value="<?php echo action_slug(DEMO_FORM); ?>" />
        <table> <!-- class="form-table" role="presentation"-->
            <?php
                render_input(DEMO_FORM, "Target Number", "demo-number");
                render_input(DEMO_FORM, "Message", "demo-message");
            ?>
            <tr>
                <td></td>
                <td>
                    <input 
                        class="button button-primary" 
                        type="submit" value="<?php _e("Send", "healy-twilio-sms-plugin"); ?>" 
                    />
                </td>
            </tr>
        </table>
    </form>
<?php }
